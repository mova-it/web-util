package mova.web;

import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class HttpUtils {

    public static final String URL_SEPARATOR = "/";

    @SuppressWarnings("unused")
    private HttpUtils() {}

    public static void printRequestParameters(ServletRequest request, PrintStream stream) {
        stream.println("Request Parameters:");
        for (String name : Collections.list(request.getParameterNames())) {
            stream.println("\t- " + name + " = " + Arrays.toString(request.getParameterValues(name)));
        }
    }

    public static void printRequestAttributes(ServletRequest request, PrintStream stream) {
        stream.println("Request Attributes:");
        for (String name : Collections.list(request.getAttributeNames())) {
            stream.println("\t- " + name + " = " + request.getAttribute(name));
        }
    }

    public static void printSessionParameters(HttpSession session, PrintStream stream) {
        stream.println("Session Parameters:");
        for (String name : Collections.list(session.getAttributeNames())) {
            stream.println("\t- " + name + " = " + session.getAttribute(name));
        }
    }

    public static Map<String, List<String>> getHeaders(HttpServletRequest request) {
        return Collections.list(request.getHeaderNames()).stream().collect(Collectors.toMap(o -> o, o -> Collections.list(request.getHeaders(o))));
    }

    public static HttpServletRequest getHttpServletRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            if (requestAttributes instanceof ServletRequestAttributes) {
                return ((ServletRequestAttributes) requestAttributes).getRequest();
            }
            if (requestAttributes instanceof NativeWebRequest) {
                return ((NativeWebRequest) requestAttributes).getNativeRequest(HttpServletRequest.class);
            }
        }
        return null;
    }

    public static HttpServletResponse getHttpServletResponse() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            if (requestAttributes instanceof ServletRequestAttributes) {
                return ((ServletRequestAttributes) requestAttributes).getResponse();
            }
            if (requestAttributes instanceof NativeWebRequest) {
                return ((NativeWebRequest) requestAttributes).getNativeResponse(HttpServletResponse.class);
            }
        }
        return null;
    }

    /**
     * Détermine s'il s'agit d'une adresse local ou loopback.
     *
     * @param url une url
     * @return true s'il s'agit d'une adresse local ou loopback, false sinon
     */
    public static boolean isLocalURL(String url) {
        Logger logger = Logger.getLogger(HttpUtils.class.getName());

        try {
            InetAddress address = InetAddress.getByName(new URL(url).getHost());
            return address.isLoopbackAddress() || address.isAnyLocalAddress();
        } catch (Exception e) {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.WARNING, e, () -> "Impossible de déterminer si l'url est local: " + url);
            } else {
                logger.log(Level.WARNING, () -> "Impossible de déterminer si l'url est local: " + url + " => " + e.getMessage());
            }
        }

        return false;
    }
}
