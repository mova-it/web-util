package mova.jsp.jstl;

import mova.util.StringUtils;

public class MovaFunctions {

    private MovaFunctions() {}

    public static String capitalize(String s) {
        return StringUtils.capitalize(s);
    }
}
