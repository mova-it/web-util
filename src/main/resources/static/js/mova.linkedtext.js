/* *********************************************************************************************
 * Pluggin linkedtext qui permet de rendre lié l'édition d'un textarea à une action (ex: ajax) *
 ***********************************************************************************************/
(function($){
    function createBtn(icon) {
        return $("<button>", {
            "type":     "button",
            "class":    "btn btn-default btn-xs"
        }).append($("<span>", {
            "class":    "glyphicon " + icon
        }));
    }

    $.fn.linkedtext = function(callback) {
        let $textarea = this;
        $textarea.toggleClass("form-control", true);
        $textarea.wrap($("<div>", {"class": "linkedtext"}));

        let $controls = $("<span>", {"class": "btn-group hidden"});
        let $btnOk = createBtn("glyphicon-ok");
        let $btnRemove = createBtn("glyphicon-remove");
        $controls.append($btnOk);
        $controls.append($btnRemove);
        $textarea.after($controls);

        $btnOk.on("mousedown", function () { /*Propage l'evenement*/ });

        $btnRemove.on("mousedown", function () {
            let oldText = $textarea.data("oldText");
            $textarea.removeData("oldText").val(oldText);
        });

        $textarea.on("focus", function () {
            $textarea.data("oldText", $textarea.val());
            $controls.xshow();
        }).on("blur", function (event) {
            let oldText = $textarea.data("oldText");
            let relatedTarget = $(event.relatedTarget).children();
            if (!relatedTarget.hasClass("glyphicon-remove")) callback(oldText);

            $textarea.removeData("oldText");
            $controls.xhide();
        });
    };
})(jQuery);