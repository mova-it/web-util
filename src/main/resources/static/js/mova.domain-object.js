if (!window["contextPath"]) {
    console.warn("mova.domain-object.js ne peut fonctionner si le contextPath n'est pas initialisé. Défaut = \"\"");
}

/* ********************************************************
 *  Object de racine générique pour les domaines          *
 **********************************************************/

const DomainObject = function(id) {

    this.id = id;

    if(typeof DomainObject.initialized === "undefined") {

        DomainObject.validator = new Validator();

        DomainObject.prototype.json = function () {
            return JSON.stringify(this);
        };

        DomainObject.prototype.equals = function (other) {
            return other instanceof this.constructor && this.json() === other.json();
        };

        DomainObject.prototype.validate = function (path) {
            return this.constructor.validator.validate(this, path);
        };

        DomainObject.prototype.save = function() {
            switch (arguments.length) {
                case 0: {
                    let domainObjectConstructor = this.constructor;
                    return $.ajax.json.post((window["contextPath"] || "") + "/" + domainObjectConstructor.domainObjectPath + "/", this, function (domainObject) {
                        // Map to correct JSObject DomainObject
                        return new domainObjectConstructor(domainObject);
                    });
                }
                case 1: {
                    if (!(arguments[0] instanceof this.constructor)) throw new SyntaxError("Usage: domainObject.save() or oldDomainObject.save(newDomainObject)");
                    let domainObject = arguments[0];
                    return $.ajax.json.put((window["contextPath"] || "") + "/" + this.constructor.domainObjectPath + "/" + this.id, domainObject);
                }
                default:    throw new SyntaxError("Usage: domainObject.save() or oldDomainObject.add(newDomainObject)");
            }
        };

        DomainObject.prototype.remove = function () {
            return $.ajax.json.delete((window["contextPath"] || "") + "/" + this.constructor.domainObjectPath + "/" + this.id);
        };

        DomainObject.prototype.toString = function() {
            return this.id;
        };

        DomainObject.initialized = true;
    }
};

DomainObject.domainObjectPath = "";

DomainObject.getAll = function (constructor) {
    return $.ajax.json.get((window["contextPath"] || "") + "/" + constructor.domainObjectPath, function(domainObjects) {
        return $.map(domainObjects, function (domainObject) {
            return new constructor(domainObject);
        });
    }).then(function(response) {
        return response[1] === "success" ? response[0] : response;
    });
};

/* ********************************************************
 *  Object de validation de formulaire                    *
 **********************************************************/
const Validator = function (validators) {
    // CONSTRUCTORS
    this.validators = validators;

    const Validations = function (validations) {
        // CONSTRUCTORS
        this.results = {};
        // La valeur de validity peut être controlée depuis les javascripts applants
        // noinspection JSUnusedGlobalSymbols
        this.validity = true;
        this.validPropertyNames = [];
        this.unvalidPropertyNames = [];

        for (let property in validations) {
            if (validations.hasOwnProperty(property)) this.add(property, validations[property]);
        }

        if(typeof Validations.initialized === "undefined") {
            // public methods
            Validations.prototype.add = function (property, validation) {
                this.results[property] = validation;

                this.validity &= validation;

                if (validation === true) this.validPropertyNames.push(property);
                else if (validation === false) {
                    this.unvalidPropertyNames.push(property);
                }
            };

            Validations.prototype.addAll = function (property, validations) {
                let $this = this;
                $.each(validations.results, function(name, validation) {
                    $this.add(property + "." + name, validation);
                });
            };

            Validations.initialized = true;
        }
    };

    if (typeof Validator.initialized === "undefined") {
        // public methods
        Validator.prototype.validate = function (object, path) {
            let validations = new Validations();

            function doValidation(path, validator) {
                // On surveille un potentiel chemin composé
                let pathParts = Path.split(path);
                // On récupère l'objet racine
                let value = Path.nestedValue(object, pathParts[0]);
                // On récupère le reste du path
                let innerPath = pathParts.length > 1 ? Path.join(pathParts.slice(1)) : null;

                // On récupère les validations du root objet désigné par le path, en tenant compte d'un potentiel chemin composé (innerPath)
                // Si Validator.TYPES.VALID et value == null ou undefined, validation = false
                // Si Validator.TYPES.NULL_OR_VALID et value = null ou undefined, validation = true
                if ((validator === Validator.TYPES.VALID || validator === Validator.TYPES.NULL_OR_VALID) && (!value || value instanceof DomainObject)) {
                    if (value) validations.addAll(pathParts[0], value.validate(innerPath));
                    else validations.add(path, validator === Validator.TYPES.NULL_OR_VALID);
                }
                // Sinon on utilise le validator passé en paramètre
                else {
                    // On récupère la valeur complète (On évite de recharger value s'il n'y a pas de chemin composé)
                    value = innerPath ? Path.nestedValue(object, pathParts) : value;
                    validations.add(path, (validator)(value, object));
                }
            }

            if (this.validators) {
                if (!path) $.each(this.validators, doValidation);
                else doValidation(path, this.validators[path]);
            }

            return validations;
        };

        Validator.initialized = true;
    }
};

Validator.TYPES = {VALID: true, NULL_OR_VALID: "NULL_OR_VALID"};