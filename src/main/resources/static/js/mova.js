// TODO Y'a eu un ajout de plein de fonction. Faudrait vérifier les ajouts et les TODO et voir si on peut pas faire du tri.
// TODO déplacer les codes d'initialisation dans une fonction anonyme
// noinspection JSUnusedGlobalSymbols

$(function () {
    // Dans un groupe de button, si le button est disabled, on empêche la propagation de l'évènement
    // De cette manière, un bouton disabled ne peut pas devenir actif
    $(".btn-group").click(function (event) {
        let $btn = $(event.target).closest(".btn");
        // Si le bouton est disabled, la fonction retourne donc false et empêche la propagation
        return !$btn.hasClass("disabled") && !$btn.prop("disabled");
    });

    // Met en place le comportement de mémorisation pour les selects qui le demandent
    $("select[data-toggle='remember']")
        .each(function() {
            let $select = $(this);
            $select.data("lastSelected", $select.find("option:selected"));
        })
        .click(function () {
            let $select = $(this);
            $select.data("lastSelected", $select.find("option:selected"));
        });
});

/* ********************************************************
 *  Adds to JQuery instance functions                     *
 **********************************************************/
// noinspection DuplicatedCode
$.extend({

    /* ********************************************************
     *  Quelques fonction diverses                            *
     **********************************************************/
    sleep:          function sleep(milliseconds) {
        let start = $.now();
        for (let i = 0; i < 1e7; i++) {
            if (($.now() - start) > milliseconds) break;
        }
    },

    /**
     * Répète nb fois la tache passé en argument.
     *
     * @param nb le nombre de fois que la tache doit être éxécutée
     * @param task la tache à éxécuter
     */
    iterate:        function(nb, task) {
        while(nb-- > 0) (task)();
    },

    /**
     * Retourne la durée de la répétition d'une fonction en vue d'obtenir un indice de performance. Par défaut, le
     * nombre d'itération est de 1000.
     *
     * @param f la fonction à tester
     * @param iteration le nombre d'itération à effectuer, 1000 sinon
     */
    perf:           function(f, iteration) {
        let startTime = $.now();
        $.iterate(iteration || 1000, f);
        let duration = $.now() - startTime;
        console.log("Duration: " + duration);

        return duration;
    },

    //TODO je sais plus comment on s'en sert. Regarder dans OSS.
    filterKey: function (event, extraControls) {
        let keyCode = event.keyCode;
        // Allow: backspace, delete, tab, escape et enter
        if ($.inArray(keyCode, [46, 8, 9, 27, 13]) !== -1
            // Allow: Ctrl+A, Command+A
            || (keyCode === 65 && (event.ctrlKey === true || event.metaKey === true))
            // Allow: Ctrl+Z, Command+Z
            || (keyCode === 90 && (event.ctrlKey === true || event.metaKey === true))
            // Allow: home, end, left, right, down, up
            || (keyCode >= 35 && keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        extraControls = Array.isArray(extraControls) ? extraControls : Array.from(arguments).slice(1);
        let hasValidControls = $.inArray(true, $.map(extraControls, function(extraControl) {
                return (extraControl)(event, keyCode, event.shiftKey);
            })) !== -1;

        if (!hasValidControls) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
    },

    /**
     * @deprecated
     */
    getEnumValue: function (obj) {
        return obj.name || obj;
    },

    getOrDefault: function (obj, key, defaultValue) {
        // si obj est défini, on renvoie la propriété obj[key]
        // sinon on renvoie la valeur par défaut si elle existe
        // sinon obj
        return obj && obj[key] || defaultValue || obj;
    },

    nullish: function(object) {
        return object === null || object === undefined;
    },

    /**
     * Vérifie si le paramètre est défini et est une chaine de caractères ni vide, ni blanche.
     * @param string le paramètre testé
     * @returns {boolean}
     */
    isNotBlank:    function (string) {
        return string !== undefined && string !== null && typeof string === "string" && string.trim().length > 0;
    },

    /**
     * Vérifie si les 2 tableaux ont au moins une valeur identique. La comparaison ce fait grâce à la fonction
     * JQuery.inArray.
     * @param array1
     * @param array2
     * @returns {boolean} retourne true s'il y a au moins une valeur identique dans les 2 tableaux, false sinon.
     */
    intersect:        function (array1, array2) {
        let hasIntersection = false;
        $.each(array1, function (index) {
            if ($.inArray(array1[index], array2) !== -1) { // !!! this != array1[index] !!!
                hasIntersection = true;
                return false;
            }
        });
        return hasIntersection;
    },

    //TODO peut être passer cette fonction dans l'extension de String.prototype
    /**
     * Retourne le complément à gauche de str avec padChar.
     * Si padChar n'est pas précisé, le caractère 0 est utilisé.
     *
     * @param str la chaine de caractère à compléter
     * @param length la longueur de la chaine de caractères retournée
     * @param padChar le caractère de complément ou 0 si undefined
     * @returns {string} le complément à gauche.
     */
    pad: function (str, length, padChar) {
        padChar = typeof padChar !== "undefined" ? padChar : "0";
        let pad = new Array(1 + length).join(padChar);
        return (pad + str).slice(-pad.length);
    },

    //TODO bouger ces fonction ?
    isChrome: function() {
        return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator["vendor"]);
    },

    isIE: function() {
        let ua = window.navigator.userAgent;

        let msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        let trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            let rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        let edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    },

    // TODO créer une extension pour la gestion de la base64
    utf8ToB64: function (utf8) {
        function uint6ToB64 (nUint6) {
            if (nUint6 < 26) return nUint6 + 65;
            if (nUint6 < 52) return nUint6 + 71;
            if (nUint6 < 62) return nUint6 - 4;
            if (nUint6 === 62) return 43;
            if (nUint6 === 63) return 47;
            return 65;
        }

        function base64EncArr (aBytes) {

            let nMod3 = 2, sB64Enc = "";

            for (let nLen = aBytes.length, nUint24 = 0, nIdx = 0; nIdx < nLen; nIdx++) {
                nMod3 = nIdx % 3;
                //On ignore cette ligne, empêche de comparer de String en base64
                //if (nIdx > 0 && (nIdx * 4 / 3) % 76 === 0) { sB64Enc += "\r\n"; }
                nUint24 |= aBytes[nIdx] << (16 >>> nMod3 & 24);
                if (nMod3 === 2 || aBytes.length - nIdx === 1) {
                    sB64Enc += String.fromCharCode(uint6ToB64(nUint24 >>> 18 & 63), uint6ToB64(nUint24 >>> 12 & 63), uint6ToB64(nUint24 >>> 6 & 63), uint6ToB64(nUint24 & 63));
                    nUint24 = 0;
                }
            }

            return sB64Enc.substring(0, sB64Enc.length - 2 + nMod3) + (nMod3 === 2 ? '' : nMod3 === 1 ? '=' : '==');

        }

        function strToUTF8Arr (sDOMStr) {

            let aBytes, nChr, nStrLen = sDOMStr.length, nArrLen = 0;

            /* mapping... */

            function getNextArrayRoomSize(nChr) {
                if (nChr < 0x80) return 1;
                if (nChr < 0x800) return 2;
                if (nChr < 0x10000) return 3;
                if (nChr < 0x200000) return 4;
                if (nChr < 0x4000000) return 5;
                return 6;
            }

            for (let nMapIdx = 0; nMapIdx < nStrLen; nMapIdx++) {
                nChr = sDOMStr.charCodeAt(nMapIdx);
                nArrLen += getNextArrayRoomSize(nChr);
            }

            aBytes = new Uint8Array(nArrLen);

            /* transcription... */

            for (let nIdx = 0, nChrIdx = 0; nIdx < nArrLen; nChrIdx++) {
                nChr = sDOMStr.charCodeAt(nChrIdx);
                if (nChr < 128) {
                    /* one byte */
                    aBytes[nIdx++] = nChr;
                } else if (nChr < 0x800) {
                    /* two bytes */
                    aBytes[nIdx++] = 192 + (nChr >>> 6);
                    aBytes[nIdx++] = 128 + (nChr & 63);
                } else if (nChr < 0x10000) {
                    /* three bytes */
                    aBytes[nIdx++] = 224 + (nChr >>> 12);
                    aBytes[nIdx++] = 128 + (nChr >>> 6 & 63);
                    aBytes[nIdx++] = 128 + (nChr & 63);
                } else if (nChr < 0x200000) {
                    /* four bytes */
                    aBytes[nIdx++] = 240 + (nChr >>> 18);
                    aBytes[nIdx++] = 128 + (nChr >>> 12 & 63);
                    aBytes[nIdx++] = 128 + (nChr >>> 6 & 63);
                    aBytes[nIdx++] = 128 + (nChr & 63);
                } else if (nChr < 0x4000000) {
                    /* five bytes */
                    aBytes[nIdx++] = 248 + (nChr >>> 24);
                    aBytes[nIdx++] = 128 + (nChr >>> 18 & 63);
                    aBytes[nIdx++] = 128 + (nChr >>> 12 & 63);
                    aBytes[nIdx++] = 128 + (nChr >>> 6 & 63);
                    aBytes[nIdx++] = 128 + (nChr & 63);
                } else /* if (nChr <= 0x7fffffff) */ {
                    /* six bytes */
                    aBytes[nIdx++] = 252 + /* (nChr >>> 32) is not possible in ECMAScript! So...: */ (nChr / 1073741824);
                    aBytes[nIdx++] = 128 + (nChr >>> 24 & 63);
                    aBytes[nIdx++] = 128 + (nChr >>> 18 & 63);
                    aBytes[nIdx++] = 128 + (nChr >>> 12 & 63);
                    aBytes[nIdx++] = 128 + (nChr >>> 6 & 63);
                    aBytes[nIdx++] = 128 + (nChr & 63);
                }
            }

            return aBytes;

        }

        return base64EncArr(strToUTF8Arr(utf8));
    },

    b64ToUTF8: function (b64) {
        function b64ToUint6 (nChr) {
            if (nChr > 64 && nChr < 91) {
                return nChr - 65;
            } else if (nChr > 96 && nChr < 123) {
                return nChr - 71;
            } else if (nChr > 47 && nChr < 58) {
                return nChr + 4;
            } else if (nChr === 43) {
                return 62;
            } else if (nChr === 47) {
                return 63;
            } else {
                return 0;
            }
        }

        function base64DecToArr (sBase64, nBlocksSize) {

            let
                sB64Enc = sBase64.replace(/[^A-Za-z0-9+/]/g, ""), nInLen = sB64Enc.length,
                nOutLen = nBlocksSize ? Math.ceil((nInLen * 3 + 1 >> 2) / nBlocksSize) * nBlocksSize : nInLen * 3 + 1 >> 2, taBytes = new Uint8Array(nOutLen);

            for (let nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
                nMod4 = nInIdx & 3;
                nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << 18 - 6 * nMod4;
                if (nMod4 === 3 || nInLen - nInIdx === 1) {
                    for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++) {
                        taBytes[nOutIdx] = nUint24 >>> (16 >>> nMod3 & 24) & 255;
                    }
                    nUint24 = 0;

                }
            }

            return taBytes;
        }

        function utf8ArrToStr (aBytes) {

            let sView = "";

            for (let nPart, nLen = aBytes.length, nIdx = 0; nIdx < nLen; nIdx++) {
                nPart = aBytes[nIdx];
                if (nPart > 251 && nPart < 254 && nIdx + 5 < nLen) {
                    sView += String.fromCharCode((nPart - 252) * 1073741824 + (aBytes[++nIdx] - 128 << 24) + (aBytes[++nIdx] - 128 << 18) + (aBytes[++nIdx] - 128 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128);
                } else if (nPart > 247 && nPart < 252 && nIdx + 4 < nLen) {
                    sView += String.fromCharCode((nPart - 248 << 24) + (aBytes[++nIdx] - 128 << 18) + (aBytes[++nIdx] - 128 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128);
                } else if (nPart > 239 && nPart < 248 && nIdx + 3 < nLen) {
                    sView += String.fromCharCode((nPart - 240 << 18) + (aBytes[++nIdx] - 128 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128);
                } else if (nPart > 223 && nPart < 240 && nIdx + 2 < nLen) {
                    sView += String.fromCharCode((nPart - 224 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128);
                } else if (nPart > 191 && nPart < 224 && nIdx + 1 < nLen) {
                    sView += String.fromCharCode((nPart - 192 << 6) + aBytes[++nIdx] - 128);
                } else {
                    sView += String.fromCharCode(nPart);
                }
            }

            return sView;

        }

        return utf8ArrToStr(base64DecToArr(b64));
    },

    // TODO une extension avec les javascript gérant le XML
    xml: {
        create: function (rootName) {
            return $.parseXML("<" + rootName + " />");
        },
        serialize: function (xmlDom) {
            let xmlString;
            if (XMLSerializer) xmlString = (new XMLSerializer()).serializeToString(xmlDom);
            else if (xmlDom.xml) xmlString = xmlDom.xml;

            return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + xmlString;
        }
    },

    xmlStreamToJSObject: function (xmlStream, callback) {
        if (!xmlStream) return null;

        let xmlString = $.b64ToUTF8(xmlStream);
        let xmlDOM = $.parseXML(xmlString);
        return callback ? callback(xmlDOM) : xmlDOM;
    },

    //TODO un polyfill de Object.values?
    values:    function(obj) {
        return $.map(obj, function(value) { return value; });
    },

    //TODO l'équivalent de window.open avec des super options?
    open: function(url, datas, windowName, windowFeatures) {
        let form = document.createElement("form");
        form.target = (windowName || "window") + $.now();
        form.method = "POST";
        form.action = url;

        if (datas) {
            $.each(datas, function () {
                let input = document.createElement("input");
                input.type = "hidden";
                input.name = this.name;
                input.value = this.value;
                form.appendChild(input);
            });
        }

        document.body.appendChild(form);

        let windowRef = window.open("", windowName, windowFeatures);

        if (!windowRef) alert("Vous devez autoriser les fenêtres pop-up pour cette action.");
        else {
            windowRef.focus();

            // On ne submit qu'une fois le formulaire
            if (!$(windowRef).data("form")) {
                $(windowRef).data("form", form);
                form.submit();
            }

            $(form).remove();

            return windowRef;
        }
    },


    /**
     * Les types de feedback utilisés par bootstrap et autorisés dans les fonctions présentes.
     */
    FEEDBACK_TYPES: {SUCCESS: "SUCCESS", WARNING: "WARNING", ERROR: "ERROR"},

    /**
     * Les types d'alert utilisés dans bootstrap et autorisé dans les fonctions présentes.
     */
    ALERT_TYPES:    {SUCCESS: "alert-success", INFO: "alert-info", WARNING: "alert-warning", DANGER: "alert-danger"},

    /**
     * Fait apparaîte un div.alert dans un div.alert-container (attaché au body). Cette alerte à pour particularité
     * d'être éphémère (4 secondes), volante et, basé sur bootstrap, permet de gérer l'apparition de message de success,
     * d'information, de prévention ou de danger.
     *
     * @param alertType Un élément de l'énumération $.ALERT_TYPES (liste des class d'alerte bootstrap)
     * @param infos Le message d'alerte
     * @param msDelay Le nombre de ms de délai
     */
    alert: function (alertType, infos, msDelay) {
        let $container = $(".alert-container").first();
        if (!$container.length) {
            $container = $("<div>", {class: "alert-container"});
            $container.css({
                position:   "fixed",
                width:      "20%",
                bottom:     "5%",
                right:      "5%",
                zIndex:     3000
            });
            $container.appendTo("body");
        }

        $("<div>", {class: "alert " + alertType})
            .html(infos)
            .hide()
            .appendTo($container)
            .fadeIn("slow")
            .delay(msDelay || 4000)
            .fadeOut("slow", function() {
                $(this).remove();

                if ($container.is(":empty")) $container.remove();
            });

        switch (alertType) {
            case $.ALERT_TYPES.SUCCESS: console.log(infos);break;
            case $.ALERT_TYPES.WARNING: console.warn(infos);break;
            case $.ALERT_TYPES.INFO:    console.info(infos);break;
            case $.ALERT_TYPES.DANGER:  console.error(infos);break;
            default:                    console.log(infos);break;
        }
    },

    /**
     * Un raccourcis pour $.alert($.ALERT_TYPES.SUCCESS, infos);
     * @param infos Le message de succes
     * @param msDelay Le nombre de ms de délai
     */
    success: function (infos, msDelay) {
        $.alert($.ALERT_TYPES.SUCCESS, infos, msDelay);
    },

    /**
     * Un raccourcis pour $.alert($.ALERT_TYPES.INFO, infos);
     * @param infos Le message d'information
     * @param msDelay Le nombre de ms de délai
     */
    info: function (infos, msDelay) {
        $.alert($.ALERT_TYPES.INFO, infos, msDelay);
    },

    /**
     * Un raccourcis pour $.alert($.ALERT_TYPES.WARNING, infos);
     * @param infos Le message de prévention
     * @param msDelay Le nombre de ms de délai
     */
    warning: function (infos, msDelay) {
        $.alert($.ALERT_TYPES.WARNING, infos, msDelay);
    },

    /**
     * Un raccourcis pour $.alert($.ALERT_TYPES.DANGER, infos);
     * @param infos Le message de danger
     * @param msDelay Le nombre de ms de délai
     */
    danger: function (infos, msDelay) {
        $.alert($.ALERT_TYPES.DANGER, infos, msDelay);
    },

    /**
     * Fait apparaître une fenêtre de confirmation modale avec un titre, un texte. Elle possède également un bouton
     * d'annulation et un bouton de confirmation au clique duquel le callback est appelé s'il est défini.
     * La fonction ne crée pas la fenêtre mais se base sur un template html repéré avec la class "confirm", comme ci
     * dessous:
     *
     * <div class="modal fade confirm">
     *     <div class="modal-dialog">
     *        <div class="modal-content">
     *            <div class="modal-header">
     *                 <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>
     *                 <h4 class="modal-title"></h4>
     *             </div>
     *             <div class="modal-body"></div>
     *             <div class="modal-footer">
     *                 <button class="btn btn-danger btn-ok" type="button">Confirmer</button>
     *                 <button class="btn btn-default" type="button" data-dismiss="modal">Annuler</button>
     *             </div>
     *         </div>
     *     </div>
     * </div>
     *
     * @param title le titre de la fenêtre
     * @param text le texte de confirmation
     * @param confirm la fonction qui sera appelée au clique du bouton de confirmation.
     * @param cancel la fonction qui sera appelée au clique du bouton d'annulation.
     */
    confirm:    function (title, text, confirm, cancel) {
        let $confirmModal = $(".modal.confirm");
        if ($confirmModal.length <= 0) return;

        // On place le titre et le text de confirmation
        $confirmModal.find(".modal-title").html(title);
        $confirmModal.find(".modal-body").html(text);

        // Si l'on clique sur le bouton de confirmation, on appelle le callback et on ferme la fenêtre modale
        $confirmModal.find(".btn-confirm").one("click", function () {
            (confirm || $.noop)();
            $confirmModal.modal("hide");
        });

        // Si l'on clique sur le bouton d'annulation, on appelle le callback et on ferme la fenêtre modale
        $confirmModal.find(".btn-cancel").one("click", function () {
            (cancel || $.noop)();
            $confirmModal.modal("hide");
        });

        // On s'assure qu'il n'y ait plus d'évènement associé aux boutons quoiqu'il arrive à la fermeture
        $confirmModal.one("hidden.bs.modal", function () {
            $confirmModal.find(".btn").off("click");
        });

        // On affiche la fenêtre modale
        $confirmModal.modal({backdrop: "static", show: true});
    },

    inform: function (title, content) {
        let $infoModal = $(".modal.information");
        if ($infoModal.length <= 0) return;

        // On place le titre et le text de confirmation
        $infoModal.find(".modal-title").html(title);
        $infoModal.find(".modal-body").html(content);

        // On affiche la fenêtre modale
        $infoModal.modal({backdrop: "static", show: true});
    }
});

$.filterKey.FILTERS = {
    DIGITS: function (event, keyCode, isShifted) {
        return isShifted && keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 105;
    }
};

/* ********************************************************
 *  Adds to JQuery functions                              *
 **********************************************************/

$.fn.extend({
    filterKey: function(delegate, extraControls) {
        // arguments is not really an array, so we use Array.from()
        if (typeof delegate === "string") {
            extraControls = Array.isArray(extraControls) ? extraControls : Array.from(arguments).slice(1);
        } else {
            extraControls = Array.isArray(delegate) ? delegate : Array.from(arguments).slice(0);
            delegate = "";
        }

        return this.on("keydown", delegate, function(event) {
            $.filterKey.call(this, event, extraControls);
        });
    },

    caretPosition:  function() {
        let textarea = this[0];

        if (!document.selection || !document.selection.createRange) return textarea.selectionStart;

        textarea.focus();

        let range = document.selection.createRange();

        let textRange = textarea.createTextRange();
        let duplicateRange = textRange.duplicate();
        textRange.moveToBookmark(range.getBookmark());
        duplicateRange.setEndPoint("EndToStart", textRange);

        return duplicateRange.text.length;
    },

    /**
     * Dans le cas d'un formulaire remet à zéro tous les inputs contenus, en retire tous les feedbacks et "désactive"
     * ces derniers en retirant la class active. Applicable également pour un ou plusieurs inputs.
     * Permet également de retirer toutes les lignes d'un tableau hormis une potentiel ligne de class "template"
     * @returns {*} L'objet JQuery
     */
    //TODO ajouter les input file dans le reset
    reset: function () {
        this.each(function () {
            let $this = $(this);
            if ($this.is("form")) {
                this.reset();
                $this.find("[type=hidden]").val("");
                $this.find("*").removeClass("has-warning has-success has-feedback has-error active danger");
                $this.find(".form-control-feedback, .help-block").xhide();
                $this.find(":checkbox").prop("checked", false);
            } else if ($this.is(":input")) {
                $this.val("");
                $this.active(false);
                $this.removeClass("danger");
                $this.feedback(false);
            }
        });

        return this;
    },

    /**
     * Sans paramètre, retourne true si l'élément est actif, false sinon. L'élément est actif si:
     * - c'est un input dont le parent direct est un objet de classe 'btn' et 'active'
     * - c'est un élément de classe 'active'
     * Avec le paramètre activated, permet d'activer ou de desactiver l'élément, c'est à dire:
     * - si c'est un input dont le parent direct est un objet de classe 'btn', on ajoute la classe 'active' à ce parent
     * - sinon on lui ajoute la classe 'active'
     * Dans le cas ou activated est renseigné, on lance l'évènement personnalisé 'active'.
     *
     * @param activated si renseigné, on ajoute la classe active, si false on retire cette dernière.
     * @returns {*} si  activated est renseigné, true, si la classe active est présente, false sinon. Dans le cas contraire
     * l'élément appelant de la fonction.
     */
    active: function (activated) {
        if (activated === undefined) {
            let $first = this.first();
            let $parent = $first.parent(".btn");
            let $target = $first.is(":input") && $parent.length > 0 ? $parent : this;
            return $target.hasClass("active");
        } else {
            return this.each(function () {
                let $this = $(this);
                let oldActivated = $this.active();
                if (oldActivated !== activated) {
                    let $parent = $this.parent(".btn");
                    let $target = $this.is(":input") && $parent.length > 0 ? $parent : $this;
                    if (activated) $target.addClass("active");
                    else $target.removeClass("active");
                    $this.trigger("active"); //TODO l'event n'est pas déclenché dans l'exemple recherche.js
                }
            });
        }
    },

    /**
     * Equivalent de val pour les checkbox et boutton radio.
     *
     * La principale différence est qu'elle retourne true/false si aucune valeur n'est précisé dans l'input.
     *
     * @returns {*}
     */
    boxVal: function () {
        let checked = this.prop("checked");
        if (!this.attr("value")) return checked;
        else if (checked) return this.val();
        else return undefined;
    },

    /**
     * Transpose le formulaire en un objet le représentant. Chaque paramètre du formulaire
     * correspondra à une entré dans l'objet du type "inputName": inputValue.
     * Cas particuliers:
     * - Les checkboxs seront traités comme des tableaux de valeur ("checkboxsName": [checkbox1value, checkbox2value, ...]).
     * - Il est possible de gérer une entré mappant un objet en utilisant le '.' dans le nom des inputs
     * (ex: inputname.option1="3" et inputname.option2="coucou" sera transposé en
     * "inputname": {"option1": 3, "option2": "coucou"}.
     * @param refs Elements sources référencés par une clé, correspondant potentiellement à une valeur stocker dans le formulaire
     * @returns {*} La transposition du formulaire, null sinon.
     */
    extract: function(refs) {
        let $form = this.first();
        if (!$form.is("form")) return null;

        let map = {};
        //On sélectionne tous les inputs du formulaire qui possèdent un nom
        let $inputs = $form.find(":input[name]");
        $inputs.each(function() {
            if ($.isNotBlank(this.name)) {
                let $input = $(this);

                let value;
                if ($input.is(":checkbox, :radio")) {
                    // Dans le cas d'un checkbox ou d'un radio, on affecte la valeur et si elle est undefined
                    // on passe à la valeur suivante. De cette manière elle ne sera pas prise en compte.
                    if ((value = $input.boxVal()) === undefined) return;
                } else {
                    // Ici this.name joue le role de catégorie et this.value d'une référence vers un objet.
                    // Cela permet d'éviter de recharger (via ajax par exemple) des objets si on les a déjà et qu'on peut
                    // y accéder via une référence.
                    // Si la référence n'est pas trouvé ou n'existe pas, on affecte la valeur telle quelle
                    value = refs && refs[this.name] ? refs[this.name][this.value] : this.value;
                }

                Path.set(this.name.split("."), map, value, $input.data("format") === "array");
            }
        });

        // On converti le résultat de l'extraction vers la classe déclaré sur le formulaire (data-class) s'il existe
        let clazz = $form.data("class");
        if (clazz) {
            try {
                // noinspection JSValidateTypes
                return new window[clazz](map);
            }
            catch (e) { console.error("Could not construct map, with class '" + clazz + "'.", e); }
        }

        return map;
    },


    /**
     * Fonction inverse de extract. Elle permet de renseigner un formulaire à partir des valeurs
     * contenues dans un objet en fonction de leur nom et de leur profondeur.
     * Exemple: Soit {nom1: 1, nom2: {nomA: "A", nomB: "B"}} renseignera dans le formulaire l'input de nom "nom1" avec
     * la valeur 1, l'input de nom "nom2.nomA" avec la valeur "A" et l'input de nom "nom2.nomB" avec la valeur "B".
     *
     * @param map L'objet à mapper dans le formulaire
     * @returns {*} Le formulaire conformément au bon usage de JQuery
     */
    feed: function (map) {
        if (!map) return this; //TODO ou peut être une exception.

        let $form = this.first();
        if (!$form.is("form")) return this; //TODO ou peut être une exception.

        $form.reset();

        // Fonction qui, par récursion, traverse la map en profondeur et renseigne les éléments nommés du formulaire
        (function objectToInput(object, namespace) {
            function toBoxes($boxes, values) {
                if (!Array.isArray(values)) values = [values];
                $.each(values, function () {
                    $boxes.filter("[value=" + this + "]").check();
                });
            }

            for (let name in object) {
                if (!object.hasOwnProperty(name)) continue
                let value = object[name];
                if ($.nullish(value)) continue;

                if ($.type(value) === "object") {
                    objectToInput(value, name);
                } else {
                    let inputName = namespace ? namespace + "." + name : name;
                    let $input = $form.find("[name='" + inputName + "']");

                    switch ($input.attr("type")) {
                        case "radio":
                        case "checkbox":    toBoxes($input, value);
                                            break;
                        default:            $input.val(value);
                    }
                }
            }
        })(map);

        $form.trigger("form.feed", map);

        return this;
    },

    //TODO Faire que cette fonction controle si l'element courrant est un template sinon le cherche dans ces descendant et retourne le clone du template
    template:   function() {
        return this.clone().removeClass("template");
    },

    /* ********************************************************
     *  Quelques fonction d'interaction IHM                   *
     **********************************************************/

    check: function (checked) {
        checked = checked === undefined ? true : checked;

        let checkedState = this.is(":checked");
        this.prop("checked", checked);

        if (checkedState !== checked) this.change();
        return this;
    },

    uncheck: function () {
        return this.check(false);
    },

    /**
     * Ajoute la class "hidden" à tous les éléments de l'objet JQuery.
     * @returns {*} L'objet JQuery
     */
    xhide: function () {
        return this.addClass("hidden");
    },

    /**
     * Retire la class "hidden" à tous les éléments de l'objet JQuery
     * @param show si show, indique si oui ou non on applique l'affichage, sinon on affiche
     * @returns {*} L'objet JQuery
     */
    xshow: function (show) {
        if (show !== undefined) return this.toggleClass("hidden", !show);
        return this.removeClass("hidden");
    },


    gotoAndFocus:   function (modifier) {
        if (this.length === 0) return this;

        let $this = this;
        let $view = $("html,body");
        if (!$view.is(":animated")) $view.animate({scrollTop: $this.offset().top + (modifier || -100)}, 200, function() {
            $this.focus();
        });

        return $this;
    },

    /**
     * Permet d'afficher ou de retirer le feedback bootstrap pour un ou plusieurs inputs, ou un input d'un formulaire.
     * Se référer aux usages possibles.
     * Usages possibles:
     * - $form.feedback(inputname, FEEDBACK_TYPE[, false])
     * - $form.feedback(inputname, false)
     * - $form.feedback(selector, FEEDBACK_TYPE[, false])
     * - $form.feedback(selector, false)
     * - $inputs.feedback(FEEDBACK_TYPE[, false])
     * - $inputs.feedback(false)
     *
     * @param selector Dans le cas d'un formulaire, le nom de l'input ou le selecteur du ou des inputs ciblés
     * @param type Le type de feedback à afficher
     * @param show si false, on cache les feedbacks, tout autre valeur est considérer comme true (affichage du feedback)
     * @returns {*} L'objet JQuery
     */
    feedback: function (selector, type, show) {
        let $inputs;
        // Si this est un formulaire
        if (this.is("form")) {
            // On récupère l'input de nom selector
            $inputs = this.find("[name='" + selector + "']");
            // S'il n'existe pas on cherche l'input correspondant au selector
            if ($inputs.length === 0) $inputs = this.find(selector);
        }
        // Sinon si this est un ou plusieurs inputs
        else if (this.is(":input")) {
            // On réaménage les arguments: feedback(type, show)
            show = type;
            type = selector;
            $inputs = this;
        } else {
            // Sinon cas hors usage
            return this;
        }

        // Si le type ne correspond pas, cas $.form.feedback(selector, false) ou $inputs.feedback(false)
        if (!$.FEEDBACK_TYPES[type]) show = false;

        // On retire les has-* et on cache les feedbacks
        let $formGroup = $inputs.parents(".form-group, .table-form");
        let $feedbacks = $formGroup.find(".help-block, .form-control-feedback");

        $formGroup.removeClass("has-success has-error has-warning has-feedback");
        $feedbacks.xhide();

        // Si show === false, on ne fait rien, sinon on affiche le feedback en fonction du type demandé
        if (show !== false) {
            switch (type) {
                case $.FEEDBACK_TYPES.SUCCESS:      $formGroup.addClass("has-success has-feedback");
                    $feedbacks.filter(".glyphicon-ok").xshow();
                    break;
                case $.FEEDBACK_TYPES.WARNING:      $formGroup.addClass("has-warning has-feedback");
                    $feedbacks.filter(".glyphicon-exclamation-sign, .help-block").xshow();

                    // Ouvre le div contenant le warning s'il y en a une
                    $formGroup.parents(".collapse").collapse("show");
                    // On affiche et on focus sur le premier élément erroné
                    $inputs.first().gotoAndFocus();
                    break;
                case $.FEEDBACK_TYPES.ERROR:        $formGroup.addClass("has-error has-feedback");
                    $feedbacks.filter(".glyphicon-remove, .help-block").xshow();

                    // Ouvre le div contenant l'erreur s'il y en a une
                    $formGroup.parents(".collapse").collapse("show");
                    // On affiche et on focus sur le premier élément erroné
                    $inputs.first().gotoAndFocus();
                    break;
            }
        }

        return this;
    },

    //TODO il faut refaire tout le système de variable des fonction de feedback: on bite rien!!
    //TODO il faudrait que le feedback fonctionne pour les inputs dans un div également
    //TODO vérifier les noms qui entreraient en conflit avec ceux de JQuery (error par exemple)

    /**
     * Usage: $form.error(selector[, show]), $form.error(inputname[, show]) ou $inputs.error([show])
     * Raccourcis de feedback avec un type $.FEEDBACK_TYPES.ERROR
     * @param selector Dans le cas d'un formulaire, le nom ou un selecteur du ou des inputs ciblés
     * @param show Si false on cache le feedback, tout autre valeur vaut true
     * @returns {*} L'objet JQuery
     */
    error: function(selector, show) {
        if (this.is("form")) return this.feedback(selector, $.FEEDBACK_TYPES.ERROR, show);
        else return this.feedback($.FEEDBACK_TYPES.ERROR, selector); // cas de error(show)
    },

    /**
     * Usage: $form.warning(selector[, show]), $form.warning(inputname[, show]) ou $inputs.warning([show])
     * Raccourcis de feedback avec un type $.FEEDBACK_TYPES.WARNING
     * @param selector Dans le cas d'un formulaire, le nom ou un selecteur du ou des inputs ciblés
     * @param show Si false on cache le feedback, tout autre valeur vaut true
     * @returns {*} L'objet JQuery
     */
    warning: function(selector, show) {
        if (this.is("form")) return this.feedback(selector, $.FEEDBACK_TYPES.WARNING, show);
        else return this.feedback($.FEEDBACK_TYPES.WARNING, selector); // cas de warning(show)
    },

    /**
     * Usage: $form.success(selector[, show]), $form.success(inputname[, show]) ou $inputs.success([show])
     * Raccourcis de feedback avec un type $.FEEDBACK_TYPES.SUCCESS
     * @param selector Dans le cas d'un formulaire, le nom ou un selecteur du ou des inputs ciblés
     * @param show Si false on cache le feedback, tout autre valeur vaut true
     * @returns {*} L'objet JQuery
     */
    success: function(selector, show) {
        if (this.is("form")) return this.feedback(selector, $.FEEDBACK_TYPES.SUCCESS, show);
        else return this.feedback($.FEEDBACK_TYPES.SUCCESS, selector); // cas de success(show)
    },

    /**
     * Cette fonction permet d'afficher une fenêtre modale contenant le formulaire qui l'appelle. Le formulaire est déplacé
     * à l'appel dans la fenêtre qui est ensuite affichée. A la fermeture, le formulaire est placé à son ancienne place.
     *
     * La fonction ne crée pas la fenêtre mais se base sur un template html repéré avec la class "crud", comme ci
     * dessous:
     * <div class="modal fade crud">
     *     <div class="modal-dialog">
     *         <div class="modal-content">
     *             <div class="modal-header">
     *                 <button type="button" class="close" data-dismiss="modal">X</button>
     *                 <h4 class="modal-title"></h4>
     *             </div>
     *             <div class="modal-body"></div>
     *             <div class="modal-footer">
     *                <button class="btn btn-info" type="submit"></button>
     *             </div>
     *         </div>
     *     </div>
     * </div>
     *
     * @param order
     * @param config config.title le titre de la fenêtre, config.btnName le nom du bouton d'action, config.submit le
     * le callback appelé lors du clique sur le bouton d'action (s'en suit la fermeture de la fenêtre, à moins que submit
     * ne retourne false), config.closed callback appelé à la fermeture de la fenêtre.
     */
    //TODO ajouter l'option validate (également pour $.fn.edit)
    crud: function(order, config) {
        if (!this.is("form")) return this; //TODO or throw an exception
        let $crudModal = $(".modal.crud"); // TODO ajouté un élément de la config pour le selecteur de modal?
        if ($crudModal.length <= 0) {
            let $modalHeader = $("<div>", {"class": "modal-header"})
            // On doit renseigner le data-dismiss ainsi pour qu'il apparaisse dans l'élément et ainsi trigger l'event (cf. bootstrap.js)
                .append($("<button>", {"class": "close", type: "button", "data-dismiss": "modal"}).text("X"))
                .append($("<h4>", {"class": "modal-title"}));
            let $modalBody = $("<div>", {"class": "modal-body"});
            let $modalFooter = $("<div>", {"class": "modal-footer"})
                .append($("<button>", {"class": "btn btn-info", type: "submit"}));

            let $modalContent = $("<div>", {"class": "modal-content"})
                .append($modalHeader)
                .append($modalBody)
                .append($modalFooter);
            let $modalDialog = $("<div>", {"class": "modal-dialog"})
                .append($modalContent);
            $crudModal = $("<div>", {"class": "modal fade crud"}).append($modalDialog);

            $crudModal.appendTo("body");
        }

        let $form = this;
        let $submit = $crudModal.find("[type=submit]");

        // TODO Mais ca sert à quoi déjà ce truc...
        function checkBlockers() {
            //TODO ajouter cette fonctionnalité à edit

            // On vérifie que rien ne bloque le submit
            let blockers = $form.data("blockers");
            if (blockers && blockers.length > 0) {
                blockers = $.grep(blockers, function (blocker) {
                    return blocker.state() === "pending";
                });

                $form.data("blockers", blockers);
                if (blockers.length > 0) {
                    $submit.disable();
                    return;
                }
            }

            $submit.enable();
        }

        if (!config) config = order;
        else if (order === "blocker.add") {
            let blocker = config;
            let blockers = $form.data("blockers");
            if (!blockers) blockers = [];
            blocker.done(checkBlockers);
            blockers.push(blocker);

            return $form.data("blockers", blockers);
        }
        else throw new SyntaxError(); //TODO compléter et corriger la javascriptdoc

        let isHiddenForm = $form.hasClass("hidden");
        // On change le titre de la fenêtre
        $crudModal.find(".modal-title").html(config.title);
        // On place une ancre pour retenir la position du formulaire
        let anchor = "ancre-" + $.now();
        $form.wrap($("<div>", {id: anchor, class: "hidden"}));

        // On appelle la fonction start si elle existe, sinon on ne fait rien
        (config.start || $.noop).call($form);
        if (isHiddenForm) $form.xshow();

        //On déplace le formulaire dans la fenêtre modale
        $crudModal.find(".modal-body").append($form.detach());

        // Le comportement de la fenêtre modale en cas de fermeture
        let submited = false;
        $crudModal.one("hidden.bs.modal", function () {
            // On replace le formulaire et on retire l'ancre
            $("#" + anchor).append($form.detach());
            $form.unwrap();

            // On appelle la fonction closed si elle existe, sinon on ne fait rien.
            (config.closed || $.noop).call($form, submited);

            if (isHiddenForm) $form.xhide();
        });

        // On tente de focus sur le premier champs visible du formulaire
        $crudModal.one("shown.bs.modal", function () {
            $form.find(":input:visible:first").focus();
        });

        // On édite le nom du bouton d'action et on place son comportement
        $submit.off("click").html(config.btnName).click(function () { $form.submit(); });

        // On place le comportement de sousmission du formulaire
        $form.off("submit").submit(function (event) {
            event.preventDefault();

            // Sur un clique et si la fonction existe, on appelle submit. On stocke la potentiel réponse.
            // Si submit existe et si la réponse est false (strictement), on retourne. Ceci peut montrer une
            // volonté délibérée d'arrêter le processus ici et de maintenit la fenêtre modale ouverte.
            let response;
            if (config.submit && (response = config.submit.call($form)) === false) return;

            submited = true;
            // S'il y a une réponse et que c'est un objet Deferred, on défère la fermeture de la fenêtre modale
            if (response && response.done) response.done(function () { $crudModal.modal("hide"); });
            // Sinon on ferme la fenêtre modale
            else $crudModal.modal("hide");
        });

        checkBlockers();

        // On affiche la fenêtre modale
        $crudModal.modal({backdrop: "static", show: true});
    },

    edit: function (config) {
        let $form = this;
        if (!$form.is("form")) return;

        let $formFooter = $form.find(".form-footer");
        if (!$formFooter.length) {
            $formFooter = $("<div>").addClass("form-footer text-right");
            $formFooter.append($("<button>").addClass("btn btn-info submit").attr("type", "button"));
            $formFooter.append(" ");// Note: On ajoute un espace pour que les buttons ne soient pas coller l'un à l'autre
            $formFooter.append($("<button>").addClass("btn btn-default cancel").attr("type", "button"));
            $form.append($formFooter);
        }

        // On appelle la fonction start si elle existe, sinon on ne fait rien
        (config.start || $.noop).call($form);

        // On montre les boutons d'edition
        $formFooter.xshow();

        // On tente de focus sur le premier champs visible du formulaire
        $form.find(":input:visible:first").focus();

        let submited = false;
        function stop() {
            // On appelle la fonction stop si elle existe, sinon on ne fait rien
            (config.stop || $.noop).call($form, submited);

            // On s'assure qu'il n'y a plus d'evenement lié au click sur les boutons
            $formFooter.find(".submit").off("click");
            $formFooter.find(".cancel").off("click");
            // On cache les boutons d'edition
            $formFooter.xhide();
        }

        // On initialise le nom du bouton submit et son comportement
        $formFooter.find(".submit").off("click").text(config.submitName).enable().click(function () {
            // Si submit n'existe pas ou si la réponse n'est pas false (strictement), on ferme l'edition.
            // Un retour false de submit permet donc de maintenir le formulaire en mode edition.
            let response;
            if (!config.submit || (response = config.submit.call($form)) !== false) {
                // On spécifie que la soumission à bien été appelée
                submited = true;
                // S'il y a une réponse et que c'est un objet Deferred, on défère l'arrêt de l'édition
                if (response && response.done) response.done(stop);
                // Sinon on arrête l'édition directement
                else stop();
            }
        });

        // Au clic sur cancel, on arrête le mode edition
        $formFooter.find(".cancel").text(config.cancelName).off("click").enable().click(stop);
    }
});

$.fn.extend((function () {
    const readonlyTypes = ["text", "search", "url", "tel", "email", "password", "date", "month", "week", "time", "datetime-local", "number"];

    function howable($element, how, able, selector) {
        if (!selector) {
            if (able !== true && able !== false) {
                selector = able;
                able = true;
            }
        }

        return $element.filter(selector || "*").each(function () {
            let $element = $(this);
            if ($element.is("textarea") || $element.is("input") && readonlyTypes.includes($element.attr("type"))) {
                $element.prop(how, !able).toggleClass(how, !able);
            } else if ($element.is("input, select, button, .btn, .thumbnail, .list-group-item, option")) {
                $element.prop("disabled", !able).toggleClass("disabled", !able);
            } else {
                howable($element.find("textarea, input, select, button, .btn, .thumbnail, .list-group-item"), how, able, selector);
            }
        });
    }

    return {
        editable: function (editable, selector) {
            return howable(this, "readonly", editable, selector);
        },

        uneditable: function (selector) {
            return this.editable(false, selector);
        },

        isEditable: function () {
            return !this.prop("readonly") && this.enabled();
        },

        enable: function (enable, selector) {
            return howable(this, "disabled", enable, selector);
        },

        disable: function (selector) {
            return this.enable(false, selector);
        },

        enabled: function () {
            return !this.prop("disabled");
        }
    }
})());

const Path = (function() {
    const PATH_PRESEPARATORS_REGEX = /[.[]/;
    const PATH_PROPERTY_SEPARATOR = ".";
    const PATH_ITEM_PRESEPARATOR = "[";
    const PATH_ITEM_POSTSEPARATOR = "]";

    return {
        /**
         * Retourne la chaine de caractère représentée par le tableau pathArray passé en paramètre.
         * Ex: [certificateInfo, account, workflowTypesAllowed, 2] devient certificateInfo.account.workflowTypesAllowed[2]
         * @param pathArray
         * @returns {*}
         */
        join: function (pathArray) {
            let path = "";

            if (!pathArray) return path;

            for (let pathPart of pathArray) {
                path += typeof pathPart === "string" ? PATH_PROPERTY_SEPARATOR + pathPart : PATH_ITEM_PRESEPARATOR + pathPart + PATH_ITEM_POSTSEPARATOR;
            }

            return typeof pathArray[0] === "number" ? path : path.substring(1);
        },

        /**
         * Split le path fournit en paramètre. (fonction inverse de join)
         * @param path
         * @returns {*}
         */
        split: function (path) {
            return $.map(path.split(PATH_PRESEPARATORS_REGEX).filter(Boolean), function (propertyName) {
                if (!propertyName.endsWith(PATH_ITEM_POSTSEPARATOR)) return propertyName;

                propertyName = propertyName.slice(0, -1);
                let index = parseInt(propertyName);
                return !isNaN(index) ? index : propertyName.slice(1, -1);
            });
        },

        /**
         * Récupère la valeur de l'objet passer en paramètre en fonction du path fourni.
         *
         * Le path peut prendre la forme d'une chaine de caractère ou d'un tableau des éléments successif du path.
         *
         * ex:
         * var o = {tralali: [[],[],[],[{},{},{patapouf:"valeur"}]]};
         * var arrayPath = ["tralali", 3, 2, "patapouf"];
         * var path = "tralali[3][2].patapouf";
         * console.log(Path.nestedValue(o, path)); // affiche "valeur"
         * console.log(Path.nestedValue(o, array)); // affiche "valeur" également
         *
         * @param o
         * @param path
         * @returns {*}
         */
        nestedValue: function (o, path) {
            if (!o || !path) return o;

            if (typeof path === "string") path = this.split(path);
            if (!Array.isArray(path) || path.length > 0 && !(typeof path[0] === "string" || typeof path[0] === "number")) {
                throw new SyntaxError("Le path ne peut être qu'une string ou un tableau de string et d'entier");
            }
            else path = $.makeArray(path);

            if (path.length === 0) return o;
            else if (path.length === 1) return o[path[0]];
            else return this.nestedValue(o[path.shift()], path);
        },


        // TODO: pour l'instant ne prend en compte qu'un tableau de chaine de caractère mais devrait pouvoir fonctionner sur la base d'un vrai path
        // (ex: path.truc[3].machin)

        // fonction récursive d'insertion de valeur. Permet notamment de mapper des objets depuis le formulaire en
        // utilisant des noms séparé par '.' comme dans l'exemple ci-dessus.
        set: function (names, o, value, forceArray) {
            let currentName = names[0];
            if (names.length === 1) {
                if (!o[currentName]) o[currentName] = forceArray ? [value] : value;
                else {
                    if (!Array.isArray(o[currentName])) o[currentName] = [o[currentName]];
                    o[currentName].push(value);
                }
            }
            else {
                if (!o[currentName]) o[currentName] = {};
                Path.set(names.slice(1), o[currentName], value)
            }
        }
    }
})();
