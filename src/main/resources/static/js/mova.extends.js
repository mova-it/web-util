//TODO Y'a eu un ajout de plein de fonction. Faudrait vérifier les ajouts et les TODO et voir si on peut pas faire du tri.
// Correction pour IE
// Create the nodeType constants if the Node object is not defined
// noinspection JSUnusedGlobalSymbols

if (!window.Node) {
    window.Node = {
        ELEMENT_NODE                :  1,
        ATTRIBUTE_NODE              :  2,
        TEXT_NODE                   :  3,
        CDATA_SECTION_NODE          :  4,
        ENTITY_REFERENCE_NODE       :  5,
        ENTITY_NODE                 :  6,
        PROCESSING_INSTRUCTION_NODE :  7,
        COMMENT_NODE                :  8,
        DOCUMENT_NODE               :  9,
        DOCUMENT_TYPE_NODE          : 10,
        DOCUMENT_FRAGMENT_NODE      : 11,
        NOTATION_NODE               : 12
    };
}

const HttpStatus = {
    OK                       :  200,
    ACCEPTED                 :  202,
    BAD_REQUEST              :  400,
    NOT_FOUND                :  404,
    FORBIDDEN                :  403,
    CONFLICT                 :  409
};

/* ********************************************************
 *  Adds to Array functions                              *
 **********************************************************/
// noinspection JSUnusedGlobalSymbols
$.extend(Array.prototype, (function () {
    const extension = {
        /**
         * Retourne le dernier élément du tableau.
         *
         * @param i si i est défini, on récupère l'élément i depuis la fin du tableau
         * @returns {*}
         */
        last:     function(i) { return this[this.length - 1 - (i || 0)]; },

        // Supprime la première occurence de l'élément passé en paramètre
        removeFirst:    function(item) {
            let indexOfFirst = this.indexOf(item);
            if (indexOfFirst === -1) return undefined;

            return this.splice(indexOfFirst, 1)[0];
        },

        empty:  function() {
            return this.length === 0;
        },

        toMap:  function (mapKey) {
            let map = {};

            this.forEach(function (value) {
                map[value[mapKey]] = value;
            });

            return map;
        },

        removeDuplicate: function () {
            let uniqueArray = [];
            this.forEach(function (value) {
                if (uniqueArray.indexOf(value) === -1) {
                    uniqueArray.push(value);
                }
            });
            return uniqueArray;
        }
    };


    // Production steps of ECMA-262, Edition 5, 15.4.4.14
    // Référence : http://es5.github.io/#x15.4.4.14
    if (!Array.prototype.indexOf) {
        // indexOf n'est pas supporté pour IE<9
        extension.indexOf = function(searchElement, fromIndex) {
            let k;

            // 1. Soit O le résultat de l'appel à ToObject avec
            //    this en argument.
            if (this === null) throw new TypeError('"this" vaut null ou n est pas défini');

            let O = Object(this);

            // 2. Soit lenValue le résultat de l'appel de la
            //    méthode interne Get de O avec l'argument
            //    "length".
            // 3. Soit len le résultat de ToUint32(lenValue).
            let len = O.length >>> 0;

            // 4. Si len vaut 0, on renvoie -1.
            if (len === 0) return -1;

            // 5. Si l'argument fromIndex a été utilisé, soit
            //    n le résultat de ToInteger(fromIndex)
            //    0 sinon
            let n = +fromIndex || 0;

            if (Math.abs(n) === Infinity) {
                n = 0;
            }

            // 6. Si n >= len, on renvoie -1.
            if (n >= len) return -1;

            // 7. Si n >= 0, soit k égal à n.
            // 8. Sinon, si n<0, soit k égal à len - abs(n).
            //    Si k est inférieur à 0, on ramène k égal à 0.
            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

            // 9. On répète tant que k < len
            while (k < len) {
                // a. Soit Pk égal à ToString(k).
                //    Ceci est implicite pour l'opérande gauche de in
                // b. Soit kPresent le résultat de l'appel de la
                //    méthode interne HasProperty de O avec Pk en
                //    argument. Cette étape peut être combinée avec
                //    l'étape c
                // c. Si kPresent vaut true, alors
                //    i.  soit elementK le résultat de l'appel de la
                //        méthode interne Get de O avec ToString(k) en
                //        argument
                //   ii.  Soit same le résultat de l'application de
                //        l'algorithme d'égalité stricte entre
                //        searchElement et elementK.
                //  iii.  Si same vaut true, on renvoie k.
                if (k in O && O[k] === searchElement) return k;
                k++;
            }
            return -1;
        };
    }

    // Production steps of ECMA-262, Edition 6, 22.1.2.1
    // Référence : https://people.mozilla.org/~jorendorff/es6-draft.html#sec-array.from
    if (!Array.from) {
        Array.from = (function () {
            const toStr = Object.prototype.toString;
            const isCallable = function (fn) {
                return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
            };
            const toInteger = function (value) {
                let number = Number(value);
                if (isNaN(number)) { return 0; }
                if (number === 0 || !isFinite(number)) { return number; }
                return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
            };
            let maxSafeInteger = Math.pow(2, 53) - 1;
            const toLength = function (value) {
                let len = toInteger(value);
                return Math.min(Math.max(len, 0), maxSafeInteger);
            };

            // La propriété length de la méthode vaut 1.
            return function from(arrayLike/*, mapFn, thisArg */) {
                // 1. Soit C, la valeur this
                let C = this;

                // 2. Soit items le ToObject(arrayLike).
                let items = Object(arrayLike);

                // 3. ReturnIfAbrupt(items).
                if (arrayLike === null) {
                    throw new TypeError("Array.from doit utiliser un objet semblable à un tableau - null ou undefined ne peuvent pas être utilisés");
                }

                // 4. Si mapfn est undefined, le mapping sera false.
                let mapFn = arguments.length > 1 ? arguments[1] : undefined;
                let T;
                if (typeof mapFn !== 'undefined') {
                    // 5. sinon
                    // 5. a. si IsCallable(mapfn) est false, on lève une TypeError.
                    if (!isCallable(mapFn)) {
                        throw new TypeError('Array.from: lorsqu il est utilisé le deuxième argument doit être une fonction');
                    }

                    // 5. b. si thisArg a été fourni, T sera thisArg ; sinon T sera undefined.
                    if (arguments.length > 2) {
                        T = arguments[2];
                    }
                }

                // 10. Soit lenValue pour Get(items, "length").
                // 11. Soit len pour ToLength(lenValue).
                let len = toLength(items.length);

                // 13. Si IsConstructor(C) vaut true, alors
                // 13. a. Soit A le résultat de l'appel à la méthode interne [[Construct]] avec une liste en argument qui contient l'élément len.
                // 14. a. Sinon, soit A le résultat de ArrayCreate(len).
                let A = isCallable(C) ? Object(new C(len)) : new Array(len);

                // 16. Soit k égal à 0.
                let k = 0;  // 17. On répète tant que k < len…
                let kValue;
                while (k < len) {
                    kValue = items[k];
                    if (mapFn) {
                        A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
                    } else {
                        A[k] = kValue;
                    }
                    k += 1;
                }
                // 18. Soit putStatus égal à Put(A, "length", len, true).
                A.length = len;  // 20. On renvoie A.
                return A;
            };
        }());
    }

    return extension;
})());

$.extend(Math, {
        randomInt: function (min, max) {
            if (!max) {
                max = min;
                min = 0;
            } else {
                min = Math.ceil(min);
            }

            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min; // NOSONAR
        }
    }
);

/* ********************************************************
 *  Adds to String functions                              *
 **********************************************************/
$.extend(String.prototype, (function () {
    const base64regex = /^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/;

    //TODO startsWith et endWith supplante la méthode originel. A modifier
    const extension = {
        /**
         * Retourne true si la chaine de caractères commence par celle passée en paramètre, false sinon. Cette fonction
         * accepte également un tableau de chaines de caractères ; dans ce cas, elle retourne true si la chaine de
         * caractères commence par au moins une de celles du tableau.
         *
         * @param starter la chaine de caractères recherchée ou un tableau des chaines de caractères recherchées
         * @returns {boolean} true si la chaine de caractères commence par starter ou au moins une des chaines du tableau,
         * false sinon
         */
        startsWith: function (starter) {
            if (starter === undefined) return false;

            let starters = (typeof starter === "string" || starter instanceof String) ? [starter] : starter;
            for (let starter of starters) if (this.indexOf(starter) === 0) return true;
            return false;
        },

        /**
         * Retourne true si la chaine de caractères termine par celle passée en paramètre, false sinon. Cette fonction
         * accepte également un tableau de chaines de caractères ; dans ce cas, elle retourne true si la chaine de
         * caractères termine par au moins une de celles du tableau.
         *
         * @param ender la chaine de caractères recherchée ou un tableau des chaines de caractères recherchées
         * @returns {boolean} true si la chaine de caractères termine par ender ou au moins une des chaines du tableau,
         * false sinon
         */
        endsWith: function (ender) {
            if (ender === undefined) return false;

            let enders = (typeof ender === "string" || ender instanceof String) ? [ender] : ender;
            for (let ender of enders) if (this.lastIndexOf(ender) === (this.length - ender.length)) return true;
            return false;
        },

        insert: function(item, position) {
            return position ? this.slice(0, position) + item + this.slice(position) : this;
        },

        capitalize: function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
        },

        isPotentiallyB64Encoded: function () {

            return base64regex.test(this) && $.utf8ToB64($.b64ToUTF8(this)) === this;
        },

        nlToBr: function() {
            return this.replace(/(?:\r\n|\r|\n)/g, "<br />");
        }
    };

    if (!String.prototype.includes) {
        /**
         * Détermine si la chaine de caractère passé en paramètre est contenu dans la chaine courante.
         *
         * @param sample la chaine recherchée.
         * @param start à partir d'un index (optionel)
         * @returns {boolean} true si la chaine recherchée est contenue dans la chaine courante, false sinon
         */
        extension.includes = function (sample, start) {
            if (sample instanceof RegExp) throw TypeError("first argument must not be a RegExp");

            if (start === undefined) { start = 0; }
            return this.indexOf(sample, start) !== -1;
        }
    }

    return extension;
})());