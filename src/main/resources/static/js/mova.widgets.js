//TODO Y'a eu un ajout de plein de fonction. Faudrait vérifier les ajouts et les TODO et voir si on peut pas faire du tri.
// noinspection JSUnusedGlobalSymbols

if ($.widget) {

    /* *************************************************************************
     * TODO a réviser: Widget Dynatab, permettant d'ajouter et de supprimer des tabs bootstrap *
     ***************************************************************************/
    $.widget("mova.dynatab", {

        options: {
            index: 0,
            startingTab: 1,
            added: $.noop
        },

        _createContent: function () {
            let $newContent = this.$tabContent.children(".template").clone().removeClass("template");
            return $newContent.attr("id", $newContent.attr("id") + this.options.index);
        },

        _createTab: function () {
            // On clone le tab template
            let $newTab = this.element.children(".template").clone().removeClass("template");
            let $newTabLink = $newTab.children();
            let newTabTitle = $newTabLink.children(".tab-title").text() + " " + this.options.index;

            // On change les propriétés du lien du tab
            $newTabLink
                .attr("href", $newTabLink.attr("href") + this.options.index)
                // On change le titre du tab
                .children(".tab-title").text(newTabTitle);

            return $newTab;
        },

        _addContent: function () {
            // Création du contenu
            let $newContent = this._createContent();

            // Mise en place du contenu
            return $newContent.appendTo(this.$tabContent);
        },

        _addTab: function () {
            let $newTab = this._createTab();

            return $newTab.insertBefore(this.$addTab).children();
        },

        _remove: function (event) {
            let context = event.data.context;

            // On garde forcément un tab
            if (context.$tabContent.children(":not(.template)").length <= 1) {
                alert("Vous ne pouvez pas supprimer le dernier pictogramme.");
                return false;
            }

            let $link = $(this).parent();
            let $tab = $link.parent();
            let anchor = $link.attr("href");

            // Si le tab que l'on supprime est actif
            if ($tab.hasClass("active")) {
                // On active le tab précédent celui que l'on supprime (le succédent si le précédent n'existe pas)
                if ($tab.prev(":not(.template)").length > 0) $tab.prev(":not(.template)").children().click();
                else $tab.next(":not(.template)").children().click();
            }

            // On supprime le tab (i.e. le tab et le contenu)
            $tab.remove();
            $(anchor).remove();
        },

        add: function (event) {
            let context = event ? event.data.context : this;

            // On récupère l'index courant
            let index = ++context.options.index;
            let $content = context._addContent();
            let $link = context._addTab().click();

            this._trigger("added", event, {content: $content, link: $link, index: index});
        },

        _create: function () {
            //TODO verifier qu'il y a un template pour le bon fonctionnement du pluggin.
            this.$tabContent = this.element.next(".tab-content");
            this.$addTab = this.element.children().last();

            // Comportement d'ajout d'un tab
            this.element.on("click", ".add-tab", {context: this}, this.add);

            // Comportement de suppression d'un tab
            this.element.on("click", ".remove-tab", {context: this}, this._remove);

            for (let i = 0; i < this.options.startingTab; i++) this.add();
        }

    });

    /* ******************************************************************************************
     * Widget table, permettant d'ajouter des lignes dans une table et de les mettre en forme *
     ********************************************************************************************/
    $.widget("mova.table", {

        options: {
            limit: -1
        },

        _DEFAULT_TRANSFORMERS: {
            "checkbox": function ($cell, value) {
                $cell.append($("<input>", {type: "checkbox", checked: value}));
            },
            "timestamp": function ($cell, value) {
                $cell.text(new Date(value).toLocaleString());
            },
            "longtext": function ($cell, value) {
                $cell.addClass("longtext").html($("<div>").text(value));
                $cell.tooltip({container: "body", title: value});
            },
            "input-hidden": function ($cell, value) {
                $cell.text(value);

                let $inputHidden = $("<input>", {
                    type: "hidden",
                    name: $cell.data("input-name"),
                    "data-format": "array",
                    value: value
                });

                $cell.append($inputHidden);
            },
            "default": function ($cell, value) {
                $cell.text(value);
            }
        },

        _DEFAULT_FOOTERS: {
            "sum": function ($footRow, $rows) {
                $footRow.children().each(function (i, cell) {
                    let $cell = $(cell);

                    if (!$cell.hasClass("disabled")) {
                        let sum = 0;
                        $.each($rows, function () {
                            sum += Number($(this).children().eq(i).text());
                        });

                        $cell.text(sum);
                    }
                });
            },
            "default": function () {}
        },

        _setRow: function ($row, rowdata) {
            let context = this;
            $row.children().each(function () {
                let $cell = $(this);
                let name = $cell.data("name");
                if (name) {
                    let names = name.split(",");
                    let value = names.length > 1 ? names.map(function (name) {
                        return Path.nestedValue(rowdata, name);
                    }) : Path.nestedValue(rowdata, names[0]);

                    if (names.length === 1 && value !== undefined && value !== null || names.length > 1 && value?.length > 0) {
                        (context.transformers[$cell.data("type") || "default"])($cell, value, rowdata);
                    }
                } else {
                    let rawEnabled = $cell.data("raw");
                    if (rawEnabled) (context.transformers[$cell.data("type") || "default"])($cell, rowdata);
                }
            });

            return $row.data("rowdata", rowdata);
        },

        _appendNewRow: function (rowdata) {
            if (this.options.limit >= 0 && this.count() >= this.options.limit) throw new RangeError();

            let $row = this.element.find("tr.template:first");
            if ($row.length === 0) return this;

            $row = $row.template();

            return this._setRow($row, rowdata).appendTo(this.element.children("tbody")).xshow();
        },

        _create: function () {
            if (!this.element.is("table")) throw new SyntaxError("docapost.table ne fonctionne qu'avec des tables.");

            this.transformers = $.extend({}, this._DEFAULT_TRANSFORMERS, this.options.transformers || {});
            this.footers = $.extend({}, this._DEFAULT_FOOTERS, this.options.footers || {});
        },

        row: function () {
            let $row;
            if ($.isNumeric(arguments[0])) {
                $row = this.element.eq(arguments[0]);
            } else if (arguments[0] instanceof jQuery && arguments[0].first().is("tr")) {
                $row = arguments[0];
            } else {
                $row = null;
            }
            let rowdata = $row ? arguments[1] : arguments[0];
            let callback = $row ? arguments[2] : arguments[1];

            if (!rowdata) return $row;
            else if (!$row) {
                $row = this._appendNewRow(rowdata);
                if (callback && $.isFunction(callback)) (callback)($row, rowdata);
                this._trigger("rowAdded", null, {$row: $row, rowdata: rowdata});

                return $row;
            }
            else {
                $row = this._setRow($row, rowdata);
                if (callback && $.isFunction(callback)) (callback)($row, rowdata);
                this._trigger("rowSet", null, {$row: $row, rowdata: rowdata});

                return $row;
            }
        },

        rows: function (rowsdata, callback) {
            if (rowsdata && !$.isFunction(rowsdata)) {
                let context = this;
                $.each(rowsdata, function () {
                    let $row = context._appendNewRow(this);
                    if (callback) (callback)($row, this);
                    context._trigger("rowAdded", null, {$row: $row, rowdata: this});
                });

                return this.element;
            } else {
                callback = $.isFunction(rowsdata) ? rowsdata : null;

                let rows = this.element.find("tbody tr:not(.template)");
                if (callback) {
                    rows.each(function () {
                        let $row = $(this);
                        (callback)($row, $row.data("rowdata"));
                    });
                }

                return rows;
            }
        },

        data: function (row) {
            return $(row).data("rowdata");
        },

        rowsData: function () {
            let $rows = this.rows();
            return $rows.length ? $.map($rows, function (row) {
                return $(row).data("rowdata");
            }) : null;
        },

        count: function (other) {
            if (!other) return this.element.find("tbody tr:not(.template)").length;

            return this.rows().filter(function (index, row) {
                let data = $(row).data("rowdata");

                if (data === other) return true;
                if (data instanceof DomainObject) return data.equals(other);

                return false;
            }).length;
        },

        remove: function ($row) {
            $row = this.element.find($row);

            if ($row) {
                let perso = this.data($row);
                $row = $row.remove();
                this._trigger("rowRemoved", null, perso);
            }

            return $row;
        },

        reset: function () {
            this.rows().remove();

            return this.element;
        },

        contains: function (other) {
            return this.count(other) > 0;
        },

        empty: function () {
            return this.count() === 0;
        },

        full: function () {
            return this.count() >= this.options.limit;
        },

        manageFooter: function () {
            let context = this;
            let $rows = this.rows();
            $.each(this.element.find("tfoot tr"), function() {
                let $footRow = $(this);
                (context.footers[$footRow.data("type") || "default"])($footRow, $rows);
            });
        },

        csv: function () {
            let $rows = this.element.find("tr:has(td, th):not(.template)");

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            let tmpColDelim = String.fromCharCode(11); // vertical tab character
            let tmpRowDelim = String.fromCharCode(0); // null character

            // actual delimiter characters for CSV format
            let colDelim = ";";
            let rowDelim = "\r\n";

            // Grab text from table into CSV formatted string
            // Prepend with the BOM
            return "\ufeff" + $rows.map(function (i, row) {
                let $cols = $(row).find("td, th");

                    return $cols.map(function (j, col) {
                        let $col = $(col);
                        return $col.text() + ($col.prop("colspan") > 1 ? tmpColDelim + new Array($col.prop("colspan") - 1).fill("").join(tmpColDelim) : "");
                    }).get().join(tmpColDelim);
                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim);
        }

    });

    /* ******************************************************************************************
     * Widget liste, permettant d'ajouter des éléments dans une liste et de les mettre en forme *
     ********************************************************************************************/
    $.widget("mova.liste", {

        _DEFAULT_TRANSFORMERS: {
            "default": function ($item, value) {
                $item.text(value);
            }
        },

        _setItem: function ($item, itemData) {
            let path = this.element.data("path");

            if (path) {
                let type = this.element.data("type");
                let value = Path.nestedValue(itemData, path);

                if (value !== undefined && value !== null) {
                    (this.transformers[type || "default"])($item, value);
                }
            }
            else console.debug("No path define for liste:", this.element);

            return $item.data("itemData", itemData);
        },

        _appendItem: function (itemData) {
            let $item = $("<li>", {
                "class": "list-group-item"
            });

            return this._setItem($item, itemData).appendTo(this.element);
        },

        _create: function () {
            if (!this.element.is("ul")) throw new SyntaxError("docapost.liste ne fonctionne qu'avec des elements <ul />.");

            this.transformers = $.extend({}, this._DEFAULT_TRANSFORMERS, this.options.transformers || {});
        },

        item: function () {
            let $item;
            if ($.isNumeric(arguments[0])) {
                $item = this.element.eq(arguments[0]);
            } else if (arguments[0] instanceof jQuery && arguments[0].first().is("li")) {
                $item = arguments[0];
            } else {
                $item = null;
            }

            let itemData = $item ? arguments[1] : arguments[0];
            let callback = $item ? arguments[2] : arguments[1];

            if (!itemData) return $item;
            else if (!$item) {
                $item = this._appendItem(itemData);
                if (callback && $.isFunction(callback)) (callback)($item, itemData);
                this._trigger("add.liste", null, {$item: $item, itemData: itemData});

                return $item;
            }
            else {
                $item = this._setItem($item, itemData);
                if (callback && $.isFunction(callback)) (callback)($item, itemData);
                this._trigger("set.liste", null, {$item: $item, itemData: itemData});

                return $item;
            }
        },

        items: function (itemsData, callback) {
            if (itemsData && !$.isFunction(itemsData)) {
                let context = this;
                $.each(itemsData, function () {
                    let $item = context._appendItem(this);
                    if (callback) (callback)($item, this);
                    context._trigger("add.liste", null, {$item: $item, itemData: this});
                });

                return this.element;
            } else {
                callback = $.isFunction(itemsData) ? itemsData : null;

                let $items = this.element.children();
                if (callback) $items.each(function () {
                    let $item = $(this);
                    (callback)($item, $item.data("itemData"));
                });

                return $items;
            }
        },

        remove: function ($item) {
            $item = this.element.find($item);

            if ($item) {
                $item = $item.remove();
                this._trigger("remove.liste");
            }

            return $item;
        },

        take: function ($item) {
            let $otherListe = $item.parent("ul");

            // On récupère les data au préalable car remove les efface de l'objet JQuery
            let data = $item.data("itemData");
            $item = $otherListe.liste("remove", $item);

            this._setItem($item, data).appendTo(this.element);
            this._trigger("add.liste", null, {$item: $item, itemData: this});
            this._trigger("take.liste");
        },

        count: function () {
            return this.element.children().length;
        },

        itemsData: function () {
            let $items = this.items();
            return $items.length ? $.map($items, function (item) {
                return $(item).data("itemData");
            }) : null;
        },

        reset: function () {
            this.items().remove();

            return this.element;
        },

        contains: function (other) {
            return this.items.find(function (item) {
                let data = $(item).data("itemData");
                if (data instanceof DomainObject) return data.equals(other);
                else return data === other;
            });
        },

        empty: function () {
            return this.count() === 0;
        }

    });
}

//*******************************************************************************************************************************************
// Box-group (des radio boutons et des checkboxs plus cool)
//*******************************************************************************************************************************************

$(function() {
    // Si l'état d'un bouton radio ou d'un checkbox change dans un box-group, on modifie également l'état du bouton parent
    $(".box-group").on("change", ".btn input:radio, .btn input:checkbox", function () {
        let $input = $(this);
        $input.parent().toggleClass("active", $input.is(":checked"));
    });

    // A l'initilisation, on fait correspondre l'état des boutons radio et des checkboxes avec leur bouton parent respectif
    $(".box-group .btn input:radio, .box-group .btn input:checkbox").each(function () {
        $(this).change();
    });
});

//TODO ce qui suit est à revoir complètement


/* ********************************************************
 *  Mise en place du comportement des spinner             *
 **********************************************************/
//TODO ce qui suit pourrais faire l'objet d'un plugin JQuery
//TODO ajouter une fonction qui initialise le spinner, plutot que de passer par $(".spinner").blur();
$(function () {
    // On commence par vérifier les valeurs defaut, mini et maxi placées sur les inputs.
    // Si des valeurs ne sont pas définies, on en attribue par défaut.
    $(".spinner + input").each(function () {
        let $this = $(this);
        let defaut = $this.data("defaut");
        $this.data("defaut", defaut !== undefined ? defaut : 0);
        let maxi = $this.data("maxi");
        $this.data("maxi", maxi !== undefined ? maxi : Number.MAX_VALUE);
        let mini = $this.data("mini");
        $this.data("mini", mini !== undefined ? mini : Number.MIN_VALUE);
    }).blur(function () {
        // On peut editer à la main l'input.
        // On blur, on vérifie la valeur editée
        let $this = $(this);
        let maxi = $this.data("maxi");
        let mini = $this.data("mini");

        // On récupère la valeur courrante
        let newVal = $this.val();
        // Si elle n'est pas défini ou NaN, on prend la valeur par défaut
        newVal = (!newVal || isNaN(newVal)) ? $this.data("defaut") : newVal;

        // On borne la valeur et on gère les boutons plus/moins en fonction
        $(".spinner .btn").enable();
        if (newVal <= mini) {
            newVal = mini;
            $(".spinner .moins-btn").disable();
            $(".spinner .plus-btn").enable();
        } else if (newVal >= maxi) {
            newVal = maxi;
            $(".spinner .plus-btn").disable();
            $(".spinner .moins-btn").enable();
        }

        $this.val(newVal);
    });

    // Fonction utilisée pour récupére l'input lié au spinner
    function getSpinnerInput($btn) {
        let $spinner = $btn.parent(".spinner");
        let $input = $spinner.next("input");
        if ($input.length === 0) $input = $spinner.prev("input");

        return $input;
    }

    // Comportement des boutons plus et moins du spinner
    let spinnerTimeoutID; //TODO peut-on se passer de cette variable? grace à $.data par exemple

    $(".spinner").on("click", ".plus-btn", function () {
        let $this = $(this);
        let $input = getSpinnerInput($this);
        // Si le bouton est enable
        if ($input.enabled()) {
            // On incrémente la valeur
            let newVal = parseInt($input.val()) + 1;
            $input.val(newVal);

            // On disable le bouton si la valeur limite est égalée
            if (newVal >= $input.data("maxi")) $this.disable();
            // On enable l'autre bouton puisque la valeur a été éloignée de sa limitte
            $this.siblings(".disabled").enable();
        }
    }).on("click", ".moins-btn", function () {
        let $this = $(this);
        let $input = getSpinnerInput($this);
        // Si le bouton est enable
        if ($input.enabled()) {
            // On décrémente la valeur
            let newVal = parseInt($input.val()) - 1;
            $input.val(newVal);

            // On disable le bouton si la valeur limite est égalée
            if (newVal <= $input.data("mini")) $this.disable();
            // On enable l'autre bouton puisque la valeur a été éloignée de sa limitte
            $this.siblings(".disabled").enable();
        }
    }).on("mousedown", ".plus-btn, .moins-btn", function () {
        let $this = $(this);
        spinnerTimeoutID = setInterval(function () {
            $this.click();
        }, 100);
    }).on("mouseup mouseleave", ".plus-btn, .moins-btn", function () {
        clearInterval(spinnerTimeoutID);
    });
});

/* ********************************************************
 *  Mise en place du comportement des inputs file         *
 **********************************************************/
/**
 * Renvoi le ou les fichiers sélectionnés (un objet File ou FileList), suivant qu'un index est fourni ou non, depuis un
 * un div de class "input-file" contenant un et un seul input de type "file".
 * Exemple:
 * <div class="input-file">
 *     <span class="btn-group">
 *         <button class="btn btn-default cancel-btn" type="button">Annuler</button>
 *         <span class="btn btn-default file-btn">
 *            Parcourir<input type="file">
 *         </span>
 *     </span>
 *     <span class="input-file-info" data-empty="Aucun fichier"></span>
 * </div>
 * @param index
 * @returns {FileList}
 */
$.fn.file = function (index) {
    //TODO un problème avec IE9
    let $this = $(this);
    if (!$this.is(".input-file")) throw new SyntaxError("Need '.input-file'");
    let files = $this.find("input")[0].files;

    if (index && (index < 0 || index >= files.length)) throw new RangeError("" + index);
    return index !== undefined ? files[index] : files;
};

$.fn.initInputFile = function () { //TODO changer rapidement le nom!
    this.children(".input-file-info").html(function () {
        return $(this).data("empty");
    });
    this.find(".file-btn input").val(null);
    this.find(".cancel-btn").disable().blur(); // blur uniquement pour retirer les pointillets moches lorsque l'on appuie sur le boutton

    return this;
};

/**
 * Remet à zéro un élément input-file, c'est à dire edite le texte de input-file-info avec le texte par défaut et la
 * valeur de l'input de type file à null.
 * Cette fonction déclenche également un évènement file.cancel.
 *
 * @returns {*} L'objet Jquery
 */
$.fn.cancel = function () {
    return this.initInputFile().trigger("file.cancel");
};

$.fn.inputfile = function () {
    let $this = $(this);
    if ($this.length === 0) return $this;
    if (!$this.is(".input-file")) throw new SyntaxError("Need '.input-file'");

    // On utilise la fonction cancel comme init de tous les input-file
    return $(".input-file").initInputFile()
        .on("change", ".file-btn input", function () {
            // En cas de choix d'un fichier, on affiche son nom dans "input-file-info"
            let $this = $(this);
            let $inputFile = $this.parents(".input-file");

            let path = $this.val();
            path = path.includes("C:\\fakepath") ? path.replace(/^.*\\/, "") : path;
            $inputFile.children(".input-file-info").html(path);
            // On rend le bouton cancel enable
            $inputFile.find(".cancel-btn").enable();

            // On signale que le fichier a changer avec l'evenement "file.change"
            $inputFile.trigger("file.change");
        })
        .on("click", ".cancel-btn", function () {
            // On appelle la fonction cancel au clique sur le bouton cancel (Logique!)
            $(this).parents(".input-file").cancel();
        });
};

// Par défaut on initialise les input-file
$(function () {
    $(".input-file").inputfile();
});