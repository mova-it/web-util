// TODO Y'a eu un ajout de plein de fonction. Faudrait vérifier les ajouts et les TODO et voir si on peut pas faire du tri.
// Fixe un bug firefox: FF warn les requetes synchrones lors de l'utilisation de $.fn.load ou $.fn.html dans une
// requête ajax. La cause une variable ajax mal initialisée.
$.ajaxPrefilter(function (options) {
    options.async = true;
});

$.ajaxSetup({cache: false});

// Configuration par défaut en cas d'erreur
$(document).ajaxError(function (event, jqxhr, options) {
    console.error(jqxhr.responseText);
    console.debug("event: ", event);
    console.debug("jqxhr: ", jqxhr);
    console.debug("options: ", options);
});

/* ********************************************************
 *  Adds to JQuery Ajax functions                         *
 **********************************************************/
// noinspection JSUnusedGlobalSymbols
$.extend($.ajax, {
    /**
     * Fonction de complaisance pour envoyer une requete ajax en définissant la configuration suivante:
     * {
     *     url:          url,
     *     method:       type,
     *     contentType:  "application/json",
     *     data:         JSON.stringify(data)
     * }
     * Puis execute la fonction then au resultat renvoyé par la requete ajax, si cette fonction est définie.
     *
     * Usage possible:
     * - $.ajax.json(url, type, data, then)
     * - $.ajax.json(url, type, data)
     * - $.ajax.json(url, type, then)
     * - $.ajax.json(url, type)
     * @param url L'url pour la requete
     * @param type Le type de requete parmi POST, GET, PUT, DELETE
     * @param data Les données à envoyer dans la requête. Ces données seront fournies au format JSON dans la requête ajax
     * @param then Le traitement a effectué sur le resultat de la requête
     * @returns {*} $.Deferred résultant de la requête
     */
    json: (function () {
        const jsonFunction = function (url, type, data, then) {
            if ($.isFunction(data)) {
                then = data;
                data = undefined;
            }

            return $.ajax({
                url: url,
                method: type,
                contentType: data ? "application/json" : false,
                data: data ? JSON.stringify(data) : undefined,
                // On ne s'attend a aucun retour en cas de DELETE et PUT (sinon, erreur dans navigateur s'il n'y a pas de body en cas d'erreur)
                mimeType: type === "DELETE" || type === "PUT" ? "text/html" : "application/json"
            }).then(function (resultData, status, jqxhr) {
                return $.Deferred().resolve(then ? then(resultData) : resultData, status, jqxhr);
            });
        };

        jsonFunction.get = function (url, data, then) {
            return $.ajax.json(url, "GET", data, then);
        };

        jsonFunction.post = function (url, data, then) {
            return $.ajax.json(url, "POST", data, then);
        };

        jsonFunction.patch = function (url, data, then) {
            return $.ajax.json(url, "PATCH", data, then);
        };

        jsonFunction.put = function (url, data, then) {
            return $.ajax.json(url, "PUT", data, then);
        };

        jsonFunction.delete = function (url, data, then) {
            return $.ajax.json(url, "DELETE", data, then);
        };

        return jsonFunction;
    })(),

    /**
     * Idem que pour json mais pour le content-type: text/plain
     */
    text: (function () {
        const textFunction = function (url, type, data, then) {
            if ($.isFunction(data)) {
                then = data;
                data = undefined;
            }

            return $.ajax({
                url: url,
                method: type,
                contentType: data ? "text/plain" : false,
                data: data
            }).then(function (resultData, status, jqxhr) {
                return $.Deferred().resolve(then ? then(resultData) : resultData, status, jqxhr);
            });
        };

        textFunction.get = function (url, data, then) {
            return $.ajax.text(url, "GET", data, then);
        };

        textFunction.post = function (url, data, then) {
            return $.ajax.text(url, "POST", data, then);
        };

        textFunction.patch = function (url, data, then) {
            return $.ajax.text(url, "PATCH", data, then);
        };

        textFunction.put = function (url, data, then) {
            return $.ajax.text(url, "PUT", data, then);
        };

        textFunction.delete = function (url, data, then) {
            return $.ajax.text(url, "DELETE", data, then);
        };

        return textFunction;
    })(),

    /**
     * Fonction de complaisance pour envoyer une requete ajax en définissant la configuration suivante:
     * {
     *     url:		       url,
     *     crossDomain:    true,
     *     method:         type,
     *     processData:    false,
     *     contentType:    false,
     *     mimeType:       "multipart/form-data",
     *     cache:          false,
     *     data:           formData,
     *     dataType:       "json"
     * }
     * Le FormData passé en paramètre est constitué des propriétés de l'objet datas de la fonction de telle manière que
     * pour une propriété nom1 de valeur valeur1, la fonction executera formData.append(nom1, valeur1).
     * Puis execute la fonction then au resultat renvoyé par la requete ajax, si cette fonction est définie.
     * Usage possible:
     * - $.ajax.multipart(url, type, datas, then)
     * - $.ajax.multipart(url, type, datas)
     * @param url L'url pour la requete
     * @param options Les options autorisées sont: type (défaut POST), dataType (défaut json) et then (Le traitement a effectué sur le resultat de la requête)
     * @param datas Les données à envoyer dans la requête. Ces données seront fournies sous la forme d'un FormData pour
     * supporter le transport de fichier. Chaque propriété de datas sera "append" au FormData
     * @returns {*} $.Deferred résultant de la requête
     */
    multipart: function (url, datas, options) {
        let formData = new FormData();
        for (let name in datas) if (datas.hasOwnProperty(name)) formData.append(name, datas[name]);
        return $.ajax({
            url:		    url,
            crossDomain:    true,
            method:         options && options.type ? options.type : "POST",
            processData:    false,
            contentType:    false,
            mimeType:       "multipart/form-data",
            cache:          false,
            data:           formData,
            dataType:       options && options.dataType ? options.dataType : "json"
        }).then(function (data, status, jqxhr) {
            return $.Deferred().resolve(options && options.then ? options.then(data) : data, status, jqxhr);
        });
    }
});